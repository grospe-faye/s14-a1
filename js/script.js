/*Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.

	Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.

	Create function which will be able to multiply two numbers.
		-Numbers must be provided as arguments.
		-Return the result of the multiplication.

	-Create a new variable called product.
		-This product should be able to receive the result of multiplication function.

	Log the value of product variable in the console.*/

//Addition function
function addNum(sum) {
	console.log(sum);
}
addNum(8 + 5);

//Subtraction function
function subtractNum(difference) {
	console.log(difference);
}
subtractNum(10 - 6);

//Multiplication function
function multiplyNum(num1, num2) {
	return num1 * num2;
}


//create new variable called product
let product = multiplyNum(5, 5);
console.log(product);